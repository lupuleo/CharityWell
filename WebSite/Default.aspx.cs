﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : Page
{
    protected void cauta(object sender, EventArgs e)
    {
        
        string sQuery;

        string s = (sender as Button).Text;
        sQuery = "select Id,Name,Image,Description from Events Order by Id desc";
        
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        SqlCommand cmd = new SqlCommand(sQuery, con);

        DataTable dt = new DataTable();
        SqlDataAdapter sda = new SqlDataAdapter();
        cmd.CommandType = CommandType.Text;
        try

        {

            con.Open();

            sda.SelectCommand = cmd;

            sda.Fill(dt);

            ListViewDEF.DataSource = dt;

            ListViewDEF.DataBind();

        }

        catch (Exception ex)

        {

            Response.Write(ex.Message);

        }

        finally

        {

            con.Close();

            sda.Dispose();

            con.Dispose();

        }

    }


   

    


    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();

        string strQuery = "select Id,Name,Image,Description from Events Order by Id desc";

        SqlCommand cmd = new SqlCommand(strQuery);

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

        SqlDataAdapter sda = new SqlDataAdapter();

        cmd.CommandType = CommandType.Text;

        cmd.Connection = con;

        try

        {

            con.Open();

            sda.SelectCommand = cmd;

            sda.Fill(dt);

            ListViewDEF.DataSource = dt;

            ListViewDEF.DataBind();

        }

        catch (Exception ex)

        {

            Response.Write(ex.Message);

        }

        finally

        {

            con.Close();

            sda.Dispose();

            con.Dispose();

        }
    }
}