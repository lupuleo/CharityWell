﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CreateEvent : System.Web.UI.Page
{
    string EventNameAUX;
    string GoalAmountAUX;
    string FileUpload1AUX;
    int tableRowContor = 1;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            EventName.Text = EventNameAUX;
            GoalAmount.Text = GoalAmountAUX;
            //FileUpload1. = FileUpload1AUX;
        }
    }
    
    //protected void AddRow(object sender, EventArgs e)
    //{
    //    TableRow row = new TableRow();

    //    TableCell cell1 = new TableCell();
    //    var lbl = new Label();
    //    lbl.Text = newAmount.Text;
    //    cell1.Width = Unit.Pixel(50);
    //    cell1.Controls.Add(lbl);
    //    //add cell to row
    //    row.Cells.Add(cell1);

    //    TableCell cell2 = new TableCell();
    //    var lbl2 = new Label();
    //    lbl2.Text = newReward.Text;
    //    cell2.Width = Unit.Pixel(50);
    //    cell2.Controls.Add(lbl2);
    //    //add cell to row
    //    row.Cells.Add(cell2);

    //    //add row to table
    //    myTable.Rows.AddAt(tableRowContor, row);
    //    tableRowContor++;

    //}
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        int eventId = CreateEventDB();
        CreateMilestoneDB(eventId);
    }

    protected int CreateEventDB()
    {
        string ID = Session["Id"].ToString();
        string FileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
        string FileNameAUX = "";
        string DescriereAUX = DescriereTemp.Text.ToString();
        string NumeAUX = EventName.Text.ToString();
        string GoalAmountAUX = GoalAmount.Text.ToString();
        

        if (FileName != "")
        {
            //Save files to disk
            FileUpload1.SaveAs(Server.MapPath("Pictures/" + FileName));
            FileNameAUX = "Pictures/" + FileName;
        }

        string query = "Insert into Events(EventName,Goal,Description,EventPicture,ONGID) VALUES (@Name,@Goal,@Description,@Picture,@ONGID); SELECT CAST(scope_identity() AS int)";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        SqlCommand cmd = new SqlCommand(query, con);

        con.Open();
        int id_inserare = 0;
        try
        {
            cmd.Parameters.AddWithValue("@Name", NumeAUX);
            cmd.Parameters.AddWithValue("@Goal", GoalAmountAUX);
            cmd.Parameters.AddWithValue("@Description", DescriereAUX);
            cmd.Parameters.AddWithValue("@Picture", FileNameAUX);
            cmd.Parameters.AddWithValue("@ONGID", ID);

             id_inserare = (int)cmd.ExecuteScalar(); // Returneaza id-ul ultimei inserari
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            con.Close();
            con.Dispose();

        }
        return id_inserare;
        
    }
    protected void CreateMilestoneDB(int eventId)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        con.Open();
        try
        {
            string query = "Insert into Milestones(EventID,Amount,Reward) VALUES (@EventId,@Amount,@Reward)";
            string amount1 = Amount1.Text.ToString();
            string reward1 = Reward1.Text.ToString();

            if (!String.IsNullOrEmpty(amount1) && !String.IsNullOrEmpty(reward1))
            {
                SqlCommand command = new SqlCommand(query, con);
                command.Parameters.AddWithValue("@EventId", eventId);
                command.Parameters.AddWithValue("@Amount", amount1);
                command.Parameters.AddWithValue("@Reward", reward1);
                command.ExecuteNonQuery();
            }

            string amount2 = Amount2.Text.ToString();
            string reward2 = Reward2.Text.ToString();

            if (!String.IsNullOrEmpty(amount2) && !String.IsNullOrEmpty(reward2))
            {
                SqlCommand command = new SqlCommand(query, con);
                command.Parameters.AddWithValue("@EventId", eventId);
                command.Parameters.AddWithValue("@Amount", amount2);
                command.Parameters.AddWithValue("@Reward", reward2);
                command.ExecuteNonQuery();
            }

            string amount3 = Amount3.Text.ToString();
            string reward3 = Reward3.Text.ToString();

            if (!String.IsNullOrEmpty(amount3) && !String.IsNullOrEmpty(reward3))
            {
                SqlCommand command = new SqlCommand(query, con);
                command.Parameters.AddWithValue("@EventId", eventId);
                command.Parameters.AddWithValue("@Amount", amount3);
                command.Parameters.AddWithValue("@Reward", reward3);
                command.ExecuteNonQuery();
            }

            string amount4 = Amount4.Text.ToString();
            string reward4 = Reward4.Text.ToString();

            if (!String.IsNullOrEmpty(amount4) && !String.IsNullOrEmpty(reward4))
            {
                SqlCommand command = new SqlCommand(query, con);
                command.Parameters.AddWithValue("@EventId", eventId);
                command.Parameters.AddWithValue("@Amount", amount4);
                command.Parameters.AddWithValue("@Reward", reward4);
                command.ExecuteNonQuery();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            con.Close();
            con.Dispose();
        }
        Response.Redirect("EventPage.aspx?a=" + eventId);
    }

    protected void CreateInitiatesDB()
    {
        string ID = Session["Id"].ToString();
        string FileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
        string FileNameAUX = "";
        string DescriereAUX = DescriereTemp.Text.ToString();
        string NumeAUX = EventName.Text.ToString();
        string GoalAmountAUX = GoalAmount.Text.ToString();        

        if (FileName != "")
        {
            //Save files to disk
            FileUpload1.SaveAs(Server.MapPath("Pictures/" + FileName));
            FileNameAUX = "Pictures/" + FileName;
        }

        string query = "Insert into Events(Name,Goal,Description,EventPicture,ONGID) VALUES (@Name,@Goal,@Description,@Picture,@ONGID)";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        SqlCommand cmd = new SqlCommand(query, con);

        con.Open();

        try
        {
            cmd.Parameters.AddWithValue("@Name", NumeAUX);
            cmd.Parameters.AddWithValue("@Goal", GoalAmountAUX);
            cmd.Parameters.AddWithValue("@Description", DescriereAUX);
            cmd.Parameters.AddWithValue("@Picture", FileNameAUX);
            cmd.Parameters.AddWithValue("@ONGID",ID);

            cmd.ExecuteNonQuery();


        }

        catch (Exception ex)

        {

            Response.Write(ex.Message);

        }

        finally

        {

            con.Close();



            con.Dispose();

        }
        Response.Redirect(Request.RawUrl);
    }
}

