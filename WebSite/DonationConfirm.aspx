﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="DonationConfirm.aspx.cs" Inherits="DonationConfirm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <style>

        body{
            
        }
    </style>
    <div id="box" style=" width:500px; height: 700px; margin-top:2px;">
     
      
       
     
       


        <div style="padding-left:20%; padding-top:3%">
             <h1 id="Name" runat="server" style=" margin-top:2px; color:cornflowerblue"></h1>
                <asp:Label runat="server" ID ="EventName" ></asp:Label> <br />
               <asp:Label ID="Amount"  style="display:inline-block; text-align:left" runat="server" />$
               
            
            <div class="form-group ">
                     
                         <asp:label for="CardNumber" runat="server"><b>Card number</b></asp:label>        <asp:Image ID="Image1" src="cards.png" width="80" Height="80" runat="server" />
                        <asp:TextBox runat="server" type="text" CssClass="form-control" id="CardNumber" aria-describedby="nameHelp" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Wrong card number" ValidationExpression="^([0-9]{4}[\s-]?){3}([0-9]{4})$" ControlToValidate="CardNumber"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="You need to enter the card number" ControlToValidate="CardNumber"></asp:RequiredFieldValidator>
                    
                        </div>
                    <div class="form-group">
                         <asp:label for="CardholderName" runat="server"><b>Cardholder Name</b></asp:label>
                        <asp:TextBox runat="server" type="text" CssClass="form-control" id="CardholderName" aria-describedby="Help1" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="You need to enter the cardholder name" ControlToValidate="CardholderName"></asp:RequiredFieldValidator>
                          <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Enter a valid name" ValidationExpression="^[a-zA-Z ]+$" ControlToValidate="CardholderName"></asp:RegularExpressionValidator>
                    </div>
                   
                        
                         
                    <div class="form-group row" style="padding-left:30px; padding-bottom:0px">
                     <div class="col-xs-2">
                 <asp:label for="ExpirationDate" runat="server"><b>Expiration Date</b></asp:label>
                        <asp:TextBox runat="server" type="text" CssClass="form-control" id="ExpirationDate" aria-describedby="Help1" placeholder="MM/YY"  />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="You need to enter the expiration date" ControlToValidate="ExpirationDate"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Wrong format." ValidationExpression="^\d{2}\/\d{2}$" ControlToValidate="ExpirationDate"></asp:RegularExpressionValidator>   
                </div>
                    <div class="col-xs-2">
    
                    <asp:label for="SecurityCode" runat="server"><b>Security Code</b></asp:label>
                    <asp:TextBox runat="server" Cssclass="form-control" id="SecurityCode" placeholder=""/>
                   <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="You need to enter the security code" ControlToValidate="SecurityCode"></asp:RequiredFieldValidator>
                         <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Security Code has 3 digits. " ValidationExpression="^[0-9]{3}$" ControlToValidate="SecurityCode"></asp:RegularExpressionValidator>       
                     </div>
                        
                         
                   </div>
            <asp:Button type="button" style="position:absolute; top:550px" Cssclass="btn btn-success btn-lg btn-block" runat="server" Text="ConfirmPayment" onClick="Pay"/>
                 </div>      
                               
        </div>
      
</asp:Content>

