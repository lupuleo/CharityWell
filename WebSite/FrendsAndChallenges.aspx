﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="FrendsAndChallenges.aspx.cs" Inherits="FrendsAndChallenges" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <style>
        #friendlist {
            width: 500px;
            height: 500px;
            background-color: white;
            overflow-y: auto;
            max-width: 500px;
            max-height: 500px;
        }

        .shadow {
            -moz-box-shadow: inset 0 0 10px #000000;
            -webkit-box-shadow: inset 0 0 10px #000000;
            box-shadow: inset 0 0 10px #000000;
        }

        hr {
            display: block;
            height: 1px;
            border: 0;
            border-top: 1px solid #ccc;
            margin: 1em 0;
            padding: 0;
        }

        .img-circle {
            border-radius: 50%;
        }

        .rcorners1 {
            border-radius: 25px;
            background: #73AD21;
            padding: 20px;
            width: 200px;
            height: 150px;
        }
    </style>
    <div class="container" style="padding-top: 5%">
        <div class="row">
            <div class="col-md-8">
                <div style="padding-left:0%; padding-top:0%;  margin-left:0px;">


                     <asp:ListView ID="ListViewChallenges" runat="server" AutoGenerateColumns="false" ShowHeader="False">
                 <LayoutTemplate>
        <table class="table" >
  <thead>
    <tr>
     
      <th scope="col">Challenge</th>
      <th scope="col">Status</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      
             <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
    
      </tbody>

        </table>
     </LayoutTemplate>
     <ItemTemplate>
         <tr>
        <td>
          <div class="row">
                <div class="col-md-4">
                       <asp:Image ImageUrl='<%#Eval("EventPicture")%>' runat="server" style="margin:10px" class="img-circle" Height="80" Width="80" />
                 </div>
                 <div class="col-md-8">
                     <div style="padding-left:10%">
                     <br />
                     '<%#Eval("Name")%>'<br />
                     '<%#Eval("EventName")%>'<br />
                     '<%#Eval("ONG")%>'<br />
                    
                   </div>
                 </div>
            </div>


      </td>
      <td>
          <div style="width: 160px; height:40px; border: 1px solid blue; margin-top:20px; border-radius: 5px; padding-top:10px; padding-left:50px;">
             '<%#Eval("Status")%>'
          </div>
          
      </td>
      <td>
          <div style="width: 160px; height:40px; border: 1px solid blue; margin-top:20px; border-radius: 5px; padding-top:2px; padding-left:50px;">
             <h5 runat="server" Onclick="ViewChallenge">View</h5> 
          </div>

      </td>
          </tr>
         
              </ItemTemplate>
         <EmptyDataTemplate>
             <tr>
            <td>
                Nothing here
            </td>
             <td>
                 
             </td>
             <td>

             </td>
                 </tr>
         </EmptyDataTemplate>      
                
            </asp:ListView>



                </div>





            </div>
            <div class="col-md-4">

                <div style="width: 400px; height: 50px">
                    <h2 style="color: cornflowerblue; text-align: center; font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif">Friends and challenges</h2>
                </div>
                <div class="row">
                    <div id="friendlist">

                        <asp:ListView ID="ListViewFriends" runat="server" AutoGenerateColumns="false" ShowHeader="False">
                            <LayoutTemplate>
                                <ul style="list-style: none; padding-top: 0;">
                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server" />

                                </ul>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <li class="rcorners1" style="background-color: #F5F5DC; width: 400px; height: 100px; margin: 1em 0; display: block; padding: 0;">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <asp:Image ImageUrl='<%#Eval("uPicture")%>' runat="server" Style="margin: 10px" class="img-circle" Height="80" Width="80" />
                                        </div>
                                        <div class="col-md-8">
                                            <%#Eval("uName")%>
                                            <br />
                                            <br />
                                            <%-- %><asp:Button runat="server" CssClass="btn btn-primary" OnCommand="Press" CommandArgument='<%#Eval("Id")%>' OnPreRender="LoadButton" Text="Buton" />
                                       --%> </div>
                                    </div>



                                </li>


                            </ItemTemplate>
                            <EmptyDataTemplate>
                                <div>
                                    <div class="rcorners1" style="background-color: #F5F5DC; width: 400px; height: 100px; margin: 1em 0; display: block; padding: 0;">There is nobody in your friends list :(  </div>
                                </div>
                            </EmptyDataTemplate>

                        </asp:ListView>



                    </div>
                </div>

            </div>
        </div>
    </div>
    <%--<div class="card">
            <div class="header">
            <div style="background-color:dodgerblue" class="shadow" width:100%; height:200>
            <h2 style="color:white; font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif">Friends and challenges</h2>
            </div>
            <div style="background-color:white"  width:100%; height:200>
            <div class="row">
                <div class="col-md-9">
                    <asp:TextBox runat="server" style="padding-left:10%; margin-right:0" CssClass="form-control" placeholder="Search ..."></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <asp:Image runat="server" onclick="Search" style="position:absolute; top:0px; left:0; " ImageUrl="Pictures/search.png" Width="40" Height="40" />
                    </div>
                
            </div>
           
            </div>
                </div>
    <div class="content">
        <ul style="list-style: none; padding-top:0;">

            <li style="background-color:#F5F5DC;  width:400px; height:100px; margin: 1em 0; display: block; padding: 0;">
                ceva aiciiii 


            </li>
            <li style="background-color:#F5F5DC; width:400px; height:100px; margin: 1em 0; display: block; padding: 0;">
            altceva ceva aiciiii 


            </li>
            <li style="background-color:#F5F5DC; width:400px; height:100px; margin: 1em 0; display: block; padding: 0;">
            inca cva ceva aiciiii 


            </li>
         
            <li style="background-color:#F5F5DC; width:400px; height:100px; margin: 1em 0; display: block; padding: 0;">
            inca cva ceva aiciiii 


            </li>
         
            <li style="background-color:#F5F5DC; width:400px; height:100px; margin: 1em 0; display: block; padding: 0;">
            inca cva ceva aiciiii 


            </li>
            <li style="background-color:#F5F5DC; width:400px; height:100px; margin: 1em 0; display: block; padding: 0;">
            inca cva ceva aiciiii 


            </li>
            <li style="background-color:#F5F5DC; width:400px; height:100px; margin: 1em 0; display: block; padding: 0;">
            inca cva ceva aiciiii 


            </li>
            <li style="background-color:#F5F5DC; width:400px; height:100px; margin: 1em 0; display: block; padding: 0;">
            inca cva ceva aiciiii 


            </li>
            <li style="background-color:#F5F5DC; width:400px; height:100px; margin: 1em 0; display: block; padding: 0;">
            inca cva ceva aiciiii 


            </li>
            <li style="background-color:#F5F5DC; width:400px; height:100px; margin: 1em 0; display: block; padding: 0;">
            inca cva ceva aiciiii 


            </li>
            <li style="background-color:#F5F5DC; width:400px; height:100px; margin: 1em 0; display: block; padding: 0;">
            inca cva ceva aiciiii 


            </li>
            <li style="background-color:#F5F5DC; width:400px; height:100px; margin: 1em 0; display: block; padding: 0;">
            inca cva ceva aiciiii 


            </li>
            <li style="background-color:#F5F5DC; width:400px; height:100px; margin: 1em 0; display: block; padding: 0;">
            inca cva ceva aiciiii 


            </li>
            <li style="background-color:#F5F5DC; width:400px; height:100px; margin: 1em 0; display: block; padding: 0;">
            inca cva ceva aiciiii 


            </li>
        </ul>
            </div>
        </div>
    <div class="main">
       
            <div class="container">
                <div class="row">


                </div>
            </div>
        </div>
    --%>
</asp:Content>

