﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="SendChallenge.aspx.cs" Inherits="SendChallenge" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
     <style>
  #friendlist{width: 500px;
                height: 500px;
                background-color:white;
                overflow-y: auto;
                max-width:500px;
                max-height:500px;
                }
  
   .shadow {
   -moz-box-shadow:    inset 0 0 10px #000000;
   -webkit-box-shadow: inset 0 0 10px #000000;
   box-shadow:         inset 0 0 10px #000000;
}
  
   hr {
    display: block;
    height: 1px;
    border: 0;
    border-top: 1px solid #ccc;
    margin: 1em 0;
    padding: 0;
}
   .img-circle {
        border-radius: 50%;
    }
    .rcorners1 {
            border-radius: 25px;
            background: #73AD21;
            padding: 20px;
            width: 200px;
            height: 150px;
        }




   </style>
    <div class="container" style="padding-top:5%">
        <div class="row">
            <div class="col-md-8">
               <div style="padding-top:2%; padding-left:20%">
                    <h2 style="color:cornflowerblue">Issue a challenge!</h2>
                   <br />

                   <div class="form-group">
                        <asp:Label runat="server" style="color:darkgrey">Event Name</asp:Label>
                        <asp:Label runat="server" ID ="EventName" Text ="Fail1"/>    
                    </div>


                    <div class="form-group">
                        <asp:Label runat="server" style="color:darkgrey">Event Name</asp:Label>
                        <asp:Label runat="server" ID ="EventName" Text ="Fail1"/>    
                    </div>
                    <div class="form-group">
                        
                        
                         <asp:Label runat="server" style="color:darkgrey" >Friend you are challening</asp:Label>
     
                         <asp:DropDownList ID="DDLFriends" runat="server" placeholder="Choose one" CssClass="form-control" ></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="You need to choose someone" ControlToValidate="DDLFriends"></asp:RequiredFieldValidator>
                    </div>
                   <div class="form-group">
                   <asp:Label runat="server" style="color:darkgrey"  >Challenge Description</asp:Label>
                     <asp:TextBox ID="ChallengeDescription" runat="server" CssClass="form-control" height="150px" TextMode="MultiLine"/>
                    </div>
                   <div class="form-group">
                   <asp:Label runat="server" style="color:darkgrey"  >Amount</asp:Label>
                     <asp:TextBox ID="Amount" runat="server" CssClass="form-control" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="You need to enter an amount" ControlToValidate="Amount"></asp:RequiredFieldValidator>
                   <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Only digits" ControlToValidate="Amount" ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>
                        
                   </div>
                   <asp:Button runat="server" CssClass="btn btn-primary btn-lg btn-block" Text="Send!" OnClick="Send" />
               </div>
               
            </div>
              <div class="col-md-4" >
                
                   <div style=" width:400px; height:50px" >
                    <h2 style="color:cornflowerblue; text-align:center;   font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif">Friends and challenges</h2>
                   </div>
                <div class="row">
                    <div id="friendlist">

                 <asp:ListView ID="ListView" runat="server" AutoGenerateColumns="false" ShowHeader="False">
                 <LayoutTemplate>
          <ul style="list-style: none; padding-top:0;">
             <asp:PlaceHolder ID="itemPlaceholder" runat="server" />

         </ul>
     </LayoutTemplate>
     <ItemTemplate>
        <li class="rcorners1" style="background-color:#F5F5DC;  width:400px; height:100px; margin: 1em 0; display: block; padding: 0;">
             <div class="row">
                 <div class="col-md-4">
                       <asp:Image ImageUrl='<%#Eval("Picture")%>' runat="server" style="margin:10px" class="img-circle" Height="80" Width="80" />
                 </div>
                 <div class="col-md-8">
                     <%#Eval("Name")%>
                     <br />
                     <br />
                     <asp:Button runat="server" CssClass="btn btn-primary" OnCommand="Press" CommandArgument='<%#Eval("Id")%>' OnPreRender="LoadButton" Text="Buton"/>
                 </div>
             </div>
           


         </li>
          
         
              </ItemTemplate>
         <EmptyDataTemplate>
             <div>
                  <div class="rcorners1" style="background-color:#F5F5DC;  width:400px; height:100px; margin: 1em 0; display: block; padding: 0;">There is nobody in your friends list :(  </div>
             </div>
         </EmptyDataTemplate>      
                
            </asp:ListView>

               

         </div>
         </div>

            </div>

        </div>
    </div>
</asp:Content>

