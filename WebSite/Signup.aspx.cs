﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Signup : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Save(object sender, EventArgs e)
    {
        string NumeAUX = Name.Text.ToString();
        string EmailAUX = Email.Text.ToString();
        string PasswordAUX = Password.Text.ToString();
        string FileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
        string FileNameAUX = "";



        if (FileName != "")
        {
                        //Save files to disk
            FileUpload1.SaveAs(Server.MapPath("Pictures/" + FileName));
            FileNameAUX = "Pictures/" + FileName;
        }

        string query = "Insert into Users(Email,Name,Password,ProfilePicture) VALUES (@Email,@Name,@Password,@ProfilePicture)";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        SqlCommand cmd = new SqlCommand(query, con);

        con.Open();

        try
        {
            cmd.Parameters.AddWithValue("@Name", NumeAUX);
            cmd.Parameters.AddWithValue("@Email", EmailAUX);
            cmd.Parameters.AddWithValue("@Password", PasswordAUX);
            cmd.Parameters.AddWithValue("@ProfilePicture", FileNameAUX);


            cmd.ExecuteNonQuery();


        }

        catch (Exception ex)

        {

            Response.Write(ex.Message);

        }

        finally

        {

            con.Close();



            con.Dispose();

        }
        Response.Redirect("Login");
    }
}