﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SiteMaster : MasterPage
{
    private const string AntiXsrfTokenKey = "__AntiXsrfToken";
    private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
    private string _antiXsrfTokenValue;

    protected void Page_Init(object sender, EventArgs e)
    {
        // The code below helps to protect against XSRF attacks
        var requestCookie = Request.Cookies[AntiXsrfTokenKey];
        Guid requestCookieGuidValue; 
        if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
        {
            // Use the Anti-XSRF token from the cookie
            _antiXsrfTokenValue = requestCookie.Value;
            Page.ViewStateUserKey = _antiXsrfTokenValue;
        }
        else
        {
            // Generate a new Anti-XSRF token and save to the cookie
            _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
            Page.ViewStateUserKey = _antiXsrfTokenValue;

            var responseCookie = new HttpCookie(AntiXsrfTokenKey)
            {
                HttpOnly = true,
                Value = _antiXsrfTokenValue
            };
            if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
            {
                responseCookie.Secure = true;
            }
            Response.Cookies.Set(responseCookie);
        }

        Page.PreLoad += master_Page_PreLoad;
    }

    protected void master_Page_PreLoad(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // Set Anti-XSRF token
            ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
            ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
        }
        else
        {
            // Validate the Anti-XSRF token
            if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
            {
                throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
    {
        Context.GetOwinContext().Authentication.SignOut();

    }
    protected void CheckLoggedBoth(object sender, EventArgs e)
    {

        if (((string)Session["Type"] == "ONG") || ((string)Session["Type"] == "User"))
        {

            LogInBtn.Visible = false;
            SettingsBtn.Visible = true;
            SignUpBtn.Visible = false;
            LogOffBtn.Visible = true;
            if((string)Session["Type"]=="User")
                FriendsandChallenges.Visible = true;

        }
        else
        {
            LogInBtn.Visible = true;
            SettingsBtn.Visible = false;
            SignUpBtn.Visible = true;
            LogOffBtn.Visible = false;
            FriendsandChallenges.Visible = false;
        }
    }
    protected void GoToSignUp(object sender,EventArgs e)
    {
        Response.Redirect("ChooseSignup.aspx");
    }

   
    protected void GoToLogIn(object sender, EventArgs e)
    {

        Response.Redirect("Login.aspx");
    }

    protected void LogOff(object sender, EventArgs e)
    {

        Session.Abandon();
        Response.Redirect("EventList.aspx");
    }
    protected void RedirectToFandC(object sender, EventArgs e)
    {
        Response.Redirect("FrendsAndChallenges");
    }

    protected void CheckONG(object sender, EventArgs e)
    {
        if ((string)Session["Type"] == "ONG")
        {

            CreateEventBtn.Visible = true;
        }
        else CreateEventBtn.Visible = false;

    }
    protected void GoToCreateEvent(object sender, EventArgs e)
    {

        Response.Redirect("CreateEvent.aspx");
    }

    protected void MasterSearch(object sender, EventArgs e)
    {
        string text = MasterSearchTextBox.Text;
        Response.Redirect("EventList?searchText=" + text);
    }
    protected void RedirectToSettings(object sender, EventArgs e)
    {
        if ((string)Session["Type"] == "ONG")
        {
            Response.Redirect("ONGSettings");
        }
        else if ((string)Session["Type"] == "User")
        {
            Response.Redirect("AccountSettings");
        }

    }
}