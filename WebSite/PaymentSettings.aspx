﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="PaymentSettings.aspx.cs" Inherits="PaymentSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
  


      <div id="box">
         <ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link" href="AccountSettings">Account</a>
  </li>
  <li class="nav-item">
    <a class="nav-link active" href="PaymentSettings">Payment Methods</a>
  </li>
  
</ul>

        
         <div style="padding:10px; width: 700px">
             
                       
                        <h2>Manage payment options</h2>
                        Any payment method you save to CharityWell will be listed here (securely) for your convenience. To save a card for future pledges, just click add a new card. 
                    <div class="form-group col-5">
                     
                         <asp:label for="CardNumber" runat="server"><b>Card number</b></asp:label>        <asp:Image ID="Image1" src="cards.png" width="80" Height="80" runat="server" />
                        <asp:TextBox runat="server" type="text" CssClass="form-control" id="CardNumber" aria-describedby="nameHelp" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter a valid card number" ValidationExpression="^([0-9]{4}[\s-]?){3}([0-9]{4})$" ControlToValidate="CardNumber"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="You need to enter the card number" ControlToValidate="CardNumber"></asp:RequiredFieldValidator>
                    
                        </div>
                    <div class="form-group">
                         <asp:label for="CardholderName" runat="server"><b>Cardholder Name</b></asp:label>
                        <asp:TextBox runat="server" type="text" CssClass="form-control" id="CardholderName" aria-describedby="Help1" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Enter a valid name" ValidationExpression="^[a-zA-Z ]+$" ControlToValidate="CardholderName"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="You need to enter the cardholder name" ControlToValidate="CardholderName"></asp:RequiredFieldValidator>
                    </div>
                   
                        
                         
                    <div class="form-group row" style="padding-left:30px; padding-bottom:0px">
                     <div class="col-xs-2">
                 <asp:label for="ExpirationDate" runat="server"><b>Expiration Date</b></asp:label>
                        <asp:TextBox runat="server" type="text" CssClass="form-control" id="ExpirationDate" aria-describedby="Help1" placeholder="MM/YY"  />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="You need to enter the expiration date" ControlToValidate="ExpirationDate"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage=" Wrong format." ValidationExpression="^(0?[1-9]|1[012])\/\d{2}$" ControlToValidate="ExpirationDate"></asp:RegularExpressionValidator>   
                </div>
                    <div class="col-xs-2">
    
                    <asp:label for="SecurityCode" runat="server"><b>Security Code</b></asp:label>
                    <asp:TextBox runat="server" Cssclass="form-control" id="SecurityCode" placeholder=""/>
                   <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="You need to enter the security code" ControlToValidate="SecurityCode"></asp:RequiredFieldValidator>
                         <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Security Code has 3 digits. " ValidationExpression="^[0-9]{3}$" ControlToValidate="SecurityCode"></asp:RegularExpressionValidator>       
                     </div>
                        
                         
                   </div>
                <asp:button runat="server" style="position:absolute; top:600px" OnClick="Save" type="button" Cssclass="btn btn-success btn-lg btn-block" Text="Save"></asp:button>
                       
            </div>                        

        </div>
       
             <%--<div class="col-md-3">
                             <div class="form-group">
                         <asp:label for="ExpirationDate" runat="server"><b>Expiration Date</b></asp:label>
                        <asp:TextBox runat="server" type="text" CssClass="form-control" id="ExpirationDate" aria-describedby="Help1" placeholder="MM/YY" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="You need to enter the expiration date" ControlToValidate="ExpirationDate"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="You need to enter the required format. The month in numbers and the last two digits of the year" ValidationExpression="^\d{2}\/\d{2}$" ControlToValidate="ExpirationDate"></asp:RegularExpressionValidator>   
                              </div>
                            </div>
                            <div class="col-md-3">
                         <div class="form-group">
                     <asp:label for="SecurityCode" runat="server"><b>Security Code</b></asp:label>
                    <asp:TextBox runat="server" Cssclass="form-control" id="SecurityCode" placeholder=""/>
                   <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="You need to enter the security code" ControlToValidate="SecurityCode"></asp:RequiredFieldValidator>
                         <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Security Code has 3 digits. " ValidationExpression="^[0-9]{3}$" ControlToValidate="SecurityCode"></asp:RegularExpressionValidator>       
                         </div>
                                </div>
                   </div>
      --%>

</asp:Content>

