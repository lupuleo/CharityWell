﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Donate.aspx.cs" Inherits="Donate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <style>
    .item {
            background-color: white;
            border: 1px solid;
            padding: 10px;
            box-shadow: 5px 10px 8px #888888;
        }
    hr {
    display: block;
    height: 1px;
    border: 0;
    border-top: 1px solid #ccc;
    margin: 1em 0;
    padding: 0;
}

    .container {
        padding-top:15px;
    }
    </style> 
    <div class="container">
       <h1 id="Name" runat="server" style="padding-left:30%; color:cornflowerblue"></h1>
        <div class="row">
            <asp:TextBox runat="server" type="text" class="form-control" ID="FreeTextAmount" aria-describedby="emailHelp" placeholder="Donate free amount" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="FreeTextAmount" ErrorMessage="Suma este obligatorie"/>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Valoarea introdusa nu este valida" Type="Currency" ControlToValidate="FreeTextAmount" Operator="DataTypeCheck" />
            <asp:Button type="button" CssClass="btn btn-succes btn-lg btn-block" runat="server" Text="Donate" OnCommand ="DonateForThis"  CommandName="DonateForThis"
        CommandArgument='none' />
        </div>
        <hr />
         <div class="row">
       <asp:ListView ID="ListView" 
        GroupItemCount="2"
        runat="server">
        <LayoutTemplate>
          <table cellpadding="2" width="640px" runat="server" id="DonationOptions">
           
            <tr runat="server" id="groupPlaceholder" />
          </table>
        </LayoutTemplate>
        <GroupTemplate>
          <tr runat="server" id="DonationsRow">
            <td runat="server" id="itemPlaceholder" />
          </tr>
        </GroupTemplate>
        <GroupSeparatorTemplate>
          <tr runat="server">
            <td colspan="3"><hr /></td>
          </tr>
        </GroupSeparatorTemplate>
        <ItemTemplate>
          <td align="center" class="item" style="width:300px " runat="server">
             <%# Eval("Amount") %>'$ 
            <hr/>
            <b>You get</b>
            <asp:Label ID="Reward" runat="server" Text='<%# Eval("Reward")%>' /><br />
         <asp:button type="button" CssClass="btn btn-succes btn-lg btn-block" runat="server" OnCommand ="DonateForThis"  CommandName="DonateForThis"
        CommandArgument='<%#Eval("Amount")%>' Text="Donate" CausesValidation="False"></asp:Button>
          </td>
        </ItemTemplate>
        <ItemSeparatorTemplate>
          <td class="separator" runat="server">&nbsp;</td>
        </ItemSeparatorTemplate>
      </asp:ListView>

           

    </div>
</div>



   
</asp:Content>

