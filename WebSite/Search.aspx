﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Search.aspx.cs" Inherits="Search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    
      
    
                <div class="container">
                 
          <h1 id="Name" runat="server" style="padding-left:30%; padding-top: 1%; color:cornflowerblue">Event List</h1>
         <br />
                   <br />
          <asp:Label Visible="false" ID="SearchWarning" CssClass="alert alert-warning" Text="You need to have at least one search criteria" runat="server" />
                    <div class="row">
       <asp:ListView ID="ListView" 
        GroupItemCount="2"
        runat="server">
        <LayoutTemplate>
          <table cellpadding="1" width="640px" runat="server" id="DonationOptions">
           
            <tr runat="server" id="groupPlaceholder" />
          </table>
        </LayoutTemplate>
        <GroupTemplate>
          <tr runat="server" id="DonationsRow">
            <td runat="server" id="itemPlaceholder" />
          </tr>
        </GroupTemplate>
        <GroupSeparatorTemplate>
          <tr runat="server">
            <td colspan="1"><hr /></td>
          </tr>
        </GroupSeparatorTemplate>
        <ItemTemplate>
            <td align="center" class="item" style="width: 300px" runat="server">
                  <div class="row">
                   
           
             <a href='EventPage.aspx?a=<%#Eval("EventID") %>'>  <%# Eval("EventName")%> <br /> </a>
             ONG:  <%# Eval("Name")%>
            </div>
                <hr />
<a href='EventPage.aspx?a=<%#Eval("EventID") %>'><img alt=""   src='<%# Eval("EventPicture") %>' height="50%" width="50%" /> </a>
                <hr />
               
              
                <asp:Button type="button" CssClass="btn btn-succes btn-lg btn-block" runat="server" OnPreRender="checkIfLogged" OnCommand="DonateForThis" CommandName="DonateForThis"
                    CommandArgument='<%#Eval("EventID")%>' Text="Donate"></asp:Button> <br />
                <asp:Button type="button" CssClass="btn btn-succes btn-lg btn-block" runat="server" OnPreRender="checkIfLogged" OnCommand="ChallengeForThis" CommandName="ChallengeForThis"
                    CommandArgument='<%#Eval("EventID")%>' Text="Challenge"></asp:Button>
            </td>
        </ItemTemplate>
        <ItemSeparatorTemplate>
          <td class="separator" runat="server">&nbsp;</td>
        </ItemSeparatorTemplate>
      </asp:ListView>

           

    </div>
                </div>
            
        
</asp:Content>

