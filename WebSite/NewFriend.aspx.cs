﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NewFriend : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Error.Visible = false;

    }
    protected void SendFriendRequest(object sender, EventArgs e)
    {
        string ID = Session["Id"].ToString();
        String query = "SELECT UserID FROM Users WHERE Email LIKE @Email";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        SqlCommand command = new SqlCommand(query,con);
        String FriendId = null;

        con.Open();
        try
        {

            command.Parameters.AddWithValue("@Email", Email.Text);
            SqlDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                Error.Text = "Mail not existing";
                Error.Visible = true;

                return;
            }
            Error.Visible = false;
            while (reader.Read())
            {
                FriendId = reader["UserID"].ToString();
            }
            con.Close();
            con.Open();
            
            query = "Insert into Friends(UserID,FriendID,Status) VALUES(@UserID,@FriendID,@Status)";
            SqlCommand command1 = new SqlCommand(query, con);
            command1.Parameters.AddWithValue("@UserID",ID);
            command1.Parameters.AddWithValue("@FriendID", FriendId);
            command1.Parameters.AddWithValue("@Status", "Pending");
            command1.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            con.Close();


        }



        string ID = Session["Id"].ToString();
        String query = "SELECT UserID FROM Users WHERE Email LIKE @Email";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        SqlCommand command = new SqlCommand(query, con);
        String FriendId = null;

        con.Open();
        try
        {

            command.Parameters.AddWithValue("@Email", Email.Text);
            SqlDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                //Response.Write("Nu exista persoana cu acest email");
                Test.Text = "Esuat";

                return;
            }
            //var FriendId = reader["UserId"].ToString();
            while (reader.Read())
            {
                FriendId = reader["UserID"].ToString();
            }
            con.Close();
            con.Open();

            query = "Insert into Friends(UserID,FriendID,Status) VALUES(@UserID,@FriendID,@Status)";
            SqlCommand command1 = new SqlCommand(query, con);
            command1.Parameters.AddWithValue("@UserID", ID);
            command1.Parameters.AddWithValue("@FriendID", FriendId);
            command1.Parameters.AddWithValue("@Status", "Pending");
            command1.ExecuteNonQuery();

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            con.Close();


        }



    }
    protected void LoadButton(object sender, EventArgs e)
    {
        Control button = (Control)sender;
    }
    protected void Press(object sender, CommandEventArgs e)
    {
        var myAction = e.CommandName as string;
        var myID = int.Parse(e.CommandArgument.ToString());
    }
}