﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AccountSettings : System.Web.UI.Page
{
    string PasswordOldAUX;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            // Luam ID-ul
            var ID = Context.User.Identity.GetUserId();
            // Salvam cererea SQL intr-un string
            string query = "SELECT Email,Password,ProfilePicture"
                    + " FROM Users"
                    + " WHERE UserID = @UserId";

            // Deschidem conexiunea la baza de date
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            con.Open();

            try
            {
                // Se construieste comanda SQL
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@UserId", ID);

                // Se executa comanda si se returneaza valorile intr-un reader
                SqlDataReader reader = com.ExecuteReader();

                // Citim rand cu rand din baza de date
                while (reader.Read())
                {
                    Email.Text = reader["Email"].ToString();
                    PasswordOldAUX = reader["Password"].ToString();
                    /*
                    Titlu.Text = reader["Titlu"].ToString();
                    Descriere.Text = reader["Descriere"].ToString();
                    Data.Text = DateTime.Parse(reader["Data"].ToString()).ToShortDateString();
                    Pret.Text = reader["Pret"].ToString();
                    DenumireOras.SelectedValue = reader["IDOras"].ToString();*/

                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }
    }
    protected void Save(object sender, EventArgs e)
    {
        var UserId = Context.User.Identity.GetUserId();
        var EmailAUX = Email.Text;
        var NewPasswordAUX = NewPassword.Text;
        var OldPasswordAUX = OldPassword.Text;  
        if (OldPasswordAUX != PasswordOldAUX) { 
            Response.Write("Parola veche este gresita");
            return;
        }
        string FileName = Path.GetFileName(FileUpload1.PostedFile.FileName);

        string query = "Update Users set Email = @Email";
        
            
        if (NewPasswordAUX != null) {
            query += ",Password = @Password";
        }
        if (FileName != null)
        {
            query += ",ProfilePicture = @FileName";
            FileUpload1.SaveAs(Server.MapPath("Pictures/" + FileName));
        }       
        query += "where UserID like @UserId";

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        SqlCommand cmd = new SqlCommand(query, con);

        con.Open();

        try
        {

            cmd.Parameters.AddWithValue("@UserId", Context.User.Identity.GetUserId());
            cmd.Parameters.AddWithValue("@Email", EmailAUX);
            if(NewPasswordAUX != null)
                cmd.Parameters.AddWithValue("@Password", NewPasswordAUX);
            if (FileName != null)
                cmd.Parameters.AddWithValue("@FileName", FileName);

            cmd.ExecuteNonQuery();


        }

        catch (Exception ex)

        {

            Response.Write(ex.Message);

        }

        finally

        {

            con.Close();



            con.Dispose();

        }
        Response.Redirect(Request.RawUrl);
    }
}