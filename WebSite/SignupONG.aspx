﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="SignupONG.aspx.cs" Inherits="SignupONG" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
     <style>
         
          #box{
              width:500px;
          }
    </style>
    <div id="box">
      
            <h3 style="padding-left: 40px; padding-top:10px">Sign up</h3>
            <div style="padding:30px">
              
                    <div class="form-group">

                        <asp:TextBox  runat="server" Cssclass="form-control" id="Name" aria-describedby="nameHelp" placeholder="Name"/>

                    </div>
                    <div class="form-group">

                        <asp:TextBox  runat="server"  Cssclass="form-control" id="Email" aria-describedby="emailHelp" placeholder="Email"/>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Email not valid" ControlToValidate="Email" ValidationExpression='^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$'></asp:RegularExpressionValidator> 
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Email is required" ControlToValidate="Email"></asp:RequiredFieldValidator>
                     </div>
                    <div class="form-group">
                        <asp:TextBox  runat="server"  Cssclass="form-control" id="ReenterEmail" placeholder="Re-enter email"/>
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Email is required" ControlToValidate="ReenterEmail"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="CompareValidator" ControlToCompare="Email" ControlToValidate="ReenterEmail"></asp:CompareValidator>
                    </div>
                    <div class="form-group">
                        <asp:TextBox runat="server"  Cssclass="form-control" id="Password" placeholder="Password" TextMode="Password" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Password is required" ControlToValidate="Password"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Minimum 8 characters" ControlToValidate="Password" ValidationExpression="^.{8,}$"></asp:RegularExpressionValidator>
                         </div>
                
                    <div class="form-group">
                        <asp:TextBox runat="server" Cssclass="form-control" id="ReenterPassword" placeholder="Re-enter Password" TextMode="Password" />
                           <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Password is required" ControlToValidate="ReenterPassword"></asp:RequiredFieldValidator>
          <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="The passwords must match" ControlToCompare="Password" ControlToValidate="ReenterPassword"></asp:CompareValidator>
                    </div>
                  <div class="form-group col-5">
                     
                         <asp:label for="Cont" runat="server"><b>IBAN</b></asp:label>       
                        <asp:TextBox runat="server" type="text" CssClass="form-control" id="Cont" aria-describedby="nameHelp" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="RegularExpressionValidator" ValidationExpression="^[a-zA-Z0-9]{24}$" ControlToValidate="Cont"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="You need to enter the account number" ControlToValidate="Cont"></asp:RequiredFieldValidator>
                    
                        </div>
                 <div class="form-group col-5">
                     
                         <asp:label for="Cui" runat="server"><b>CUI</b></asp:label>       
                        <asp:TextBox runat="server" type="text" CssClass="form-control" id="Cui" aria-describedby="nameHelp" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Cui-ul e intre 6-9 cifre" ValidationExpression="^[0-9]{6,9}$" ControlToValidate="Cui"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="You need to enter the CUI" ControlToValidate="Cui"></asp:RequiredFieldValidator>
                    
                        </div>
                    
                    <asp:Button runat="server" id="signupbtn"  onclick="Save" Cssclass="btn btn btn-success btn-lg btn-block" Text="Sign me up!" ></asp:Button>
             
              
            </div>
            

        </div>

</asp:Content>

