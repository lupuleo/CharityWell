﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EventPage : System.Web.UI.Page
{
    string photoid;
    int pu;
    int pd;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            var id = Request.QueryString["a"];



            string strQuery = "select EventID,Goal,Name, Backers, Pledged, EventName,Description,EventPicture from Events e join ONGs o on (e.ONGID = o.ONGID) where EventID like @Id;";
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            SqlCommand cmd = new SqlCommand(strQuery, con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@Id", id);
            con.Open();

            using (SqlDataReader rdr = cmd.ExecuteReader())
            {

                if (rdr.HasRows)
                {
                    rdr.Read();
                    Name.InnerText = rdr["EventName"].ToString();
                    Description.InnerText = rdr["Description"].ToString();
                    Image.ImageUrl = rdr["EventPicture"].ToString();
                    ONG.InnerText = rdr["Name"].ToString();
                    Backers.Text = rdr["Backers"].ToString();
                    Current.InnerText = rdr["Pledged"].ToString();

                    Target.InnerText = rdr["Goal"].ToString();




                }
            }


            con.Close();
            List<String> rewards = new List<String>();

            //don't forget to do this for the list view. there should be a table with the rewards for each event. I think it should be a mnay to many relation.  

            ListView.DataSource = rewards;

           ListView.DataBind();

        }
    }
    protected void Donate(object sender, EventArgs e)
    {


    }
    protected void Challenge(object sender, EventArgs e)
    {

    }

    protected void DonateForThis(object sender, EventArgs e)
    {
        var id = Request.QueryString["a"];

        if ((string)Session["Type"] == "User")
            Response.Redirect("Donate.aspx?event=" + id);

        else Response.Redirect("Signup");
    }
    protected void checkIfLogged(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        if (!((string)Session["Type"] == "User"))
        {

            btn.Visible = false;
        }
        else btn.Visible = true;

    }
    protected void ChallengeForThis(object sender, EventArgs e)
    {

        var id = Request.QueryString["a"];
        Response.Redirect("SendChallenge.aspx?EventId=" + id);

    }

}