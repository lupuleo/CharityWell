﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

     <div id="box">
           <p id="WrongData" runat="server" visible="false"  style="margin-left: 30px; padding-left:40px; width:300px;" class="alert alert-danger" >Wrong username or password!</p>
        <p id="ChooseOne" runat="server" visible="false"  style="margin-left: 30px; padding-left:40px; width:300px;" class="alert alert-warning" >Choose one log in option</p>
           <h3 style="padding-left: 40px; padding-top:10px">Log in</h3>
        <div style="padding:30px">
               
                <div class="form-group">

                    <asp:TextBox type="email" runat="server" EnableCLientScript="false" id="InputEmail" CssClass="form-control" aria-describedby="emailHelp" placeholder="Email"/>

                </div>
                <div class="form-group">
                    <asp:TextBox type="password"  runat="server" class="form-control" id="InputPassword" placeholder="Password"/>
                </div>
                
                
            <div class="custom-control custom-radio">
   <asp:RadioButtonList id="RadioButtonList" runat="server" RepeatDirection="Horizontal">
            <asp:ListItem style="padding-right:10px">Log in as User</asp:ListItem>
            <asp:ListItem>Log in as ONG</asp:ListItem>
         
          </asp:RadioButtonList>  
                <br />  
                <br />    
      <asp:button type="button" runat="server" id="loginbtn" onclick="LoginClick" CssClass="btn btn btn-success btn-lg" Text="Log me in!"></asp:button>
              
           
        </div>
       
             <div style= "padding-top:10px">New to Charity well? <a href="ChooseSignup">Sign up!</a></div>
  
       

    </div>
         </div>

</asp:Content>

