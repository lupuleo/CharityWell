﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeFile="AccountSettings.aspx.cs" Inherits="AccountSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
   
        <div id="box">
  
           
  <ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link active" href="#">Account</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="PaymentSettings">Payment Methods</a>
  </li>
  
</ul>
 <div style="padding:30px">
                    <div style ="width:300px" >
                    <div class="form-group">
                         <asp:label for="Email" runat="server"><b>Email</b></asp:label>
                        <asp:TextBox runat="server" type="text" CssClass="form-control" id="Email" aria-describedby="nameHelp" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Wrong email format" ControlToValidate="Email" ValidationExpression="^([\w.-]+)@([\w-]+)((.(\w){2,3})+)$"></asp:RegularExpressionValidator>

                    </div>
                    <div class="form-group">
                        <asp:FileUpload ID="FileUpload1" runat="server" cssClass="form-control"/>
                    </div>
                    <div class="form-group">
                         <asp:label for="NewPassword" runat="server"><b>New Password</b></asp:label>
                        <asp:TextBox runat="server" type="text" CssClass="form-control" id="NewPassword" aria-describedby="Help1" />
                         <small id="Help1" class="form-text text-muted">Minimum 8 characters</small>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Minimum 8 characters" ControlToValidate="NewPassword" ValidationExpression="/^(?=.*\d).{8,}$/"></asp:RegularExpressionValidator>
                    </div>
                     <div class="form-group">
                         <asp:label for="ConfirmPassword" runat="server"><b>Confirm Password</b></asp:label>
                        <asp:TextBox runat="server" type="text" CssClass="form-control" id="ConfirmPassword" aria-describedby="Help2" />
                           <small id="Help2" class="form-text text-muted">Make sure they match!</small>
                         <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="The passwords must match" ControlToCompare="NewPassword" ControlToValidate="ConfirmPassword"></asp:CompareValidator>
                    </div>
                          <div class="form-group">
                         <asp:label for="OldPassword" runat="server"><b>Old Password</b></asp:label>
                        <asp:TextBox runat="server" type="text" CssClass="form-control" id="OldPassword" aria-describedby="Help3" />
                         <small id="Help3" class="form-text text-muted">Enter your current password to save the changes</small>
                              <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="You need to input your old password to make any changes" ControlToValidate="OldPassword"></asp:RequiredFieldValidator>


                          </div>
                        
                        <asp:button runat="server" OnClick="Save" type="button" CssClass="btn btn-primary btn-lg btn-block" Text="Save"></asp:button>
                        </div> 

        </div>
    </div>
  
</asp:Content>
