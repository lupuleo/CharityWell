﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ChooseSignup.aspx.cs" Inherits="ChooseSignup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
     <div id="box">
         <h2>Sign Up</h2> <br />
         <br />
         <asp:Button runat="server" OnClick="GoToSignUpUser" CssClass="btn btn-primary btn-lg" CausesValidation="false"  ID="SignUpUser" Text="Sign up for an user account" /> <br /> <br />
         <asp:Button runat="server" OnClick="GoToSignUpONG" CssClass="btn btn-primary btn-lg" CausesValidation="false"  ID="SignUpONG" Text="Sign up for an ONG account" />



    </div>
</asp:Content>

