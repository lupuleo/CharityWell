﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ONGSettings.aspx.cs" Inherits="ONGSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

      <div id="box">
          <div style="padding:30px">
                    <div style ="width:300px" >
                    <div class="form-group">
                         <asp:label for="Email" runat="server"><b>Email</b></asp:label>
                        <asp:TextBox runat="server" type="text" CssClass="form-control" id="Email" aria-describedby="nameHelp" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Wrong email format" ControlToValidate="Email" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"></asp:RegularExpressionValidator>
                    </div>
                     <div class="form-group">
                         <asp:label for="NewPassword" runat="server"><b>New Password</b></asp:label>
                        <asp:TextBox runat="server" type="text" CssClass="form-control" id="NewPassword" aria-describedby="Help1" />
                         <small id="Help1" class="form-text text-muted">Minimum 8 characters</small>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Minimum 8 characters" ControlToValidate="NewPassword" ValidationExpression="/^(?=.*\d).{8,}$/"></asp:RegularExpressionValidator>
                    </div>
                     <div class="form-group">
                         <asp:label for="ConfirmPassword" runat="server"><b>Confirm Password</b></asp:label>
                        <asp:TextBox runat="server" type="text" CssClass="form-control" id="ConfirmPassword" aria-describedby="Help2" />
                           <small id="Help2" class="form-text text-muted">Make sure they match!</small>
                         <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="The passwords must match" ControlToCompare="NewPassword" ControlToValidate="ConfirmPassword"></asp:CompareValidator>
                    </div>
                        <div class="form-group col-5">
                     
                         <asp:label for="Cont" runat="server"><b>IBAN</b></asp:label>       
                        <asp:TextBox runat="server" type="text" CssClass="form-control" id="Cont" aria-describedby="nameHelp" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="RegularExpressionValidator" ValidationExpression="^[a-zA-Z0-9]{24}$" ControlToValidate="Cont"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="You need to enter the account number" ControlToValidate="Cont"></asp:RequiredFieldValidator>
                    
                        </div>
                          <div class="form-group">
                         <asp:label for="OldPassword" runat="server"><b>Old Password</b></asp:label>
                        <asp:TextBox runat="server" type="text" CssClass="form-control" id="OldPassword" aria-describedby="Help3" />
                         <small id="Help3" class="form-text text-muted">Enter your current password to save the changes</small>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="You need to input your old password to make any changes" ControlToValidate="OldPassword"></asp:RequiredFieldValidator>


                          </div>
                        <asp:button runat="server" OnClick="Save" type="button" CssClass="btn btn-primary btn-lg btn-block" Text="Save"></asp:button>
                        </div> 

                    </div>

           </div>
    
</asp:Content>

