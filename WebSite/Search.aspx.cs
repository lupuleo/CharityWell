﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Search : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        

    }
    
    protected void DonateForThis(object sender, CommandEventArgs e)
    {
        var myID = int.Parse(e.CommandArgument.ToString());

        if ((string)Session["Type"] == "User")
            Response.Redirect("DonationConfirm.aspx?EventId=" + myID);
        else Response.Redirect("Signup");
    }
    protected void ChallengeForThis(object sender, CommandEventArgs e)
    {

        var myAction = e.CommandName as string;
        var myID = int.Parse(e.CommandArgument.ToString());

        //Response.Redirect("Challenge.aspx?EventId=" + myID);
    }

    protected void checkIfLogged(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        if (!((string)Session["Type"] == "User" || (string)Session["Type"] == "ONG"))
        {

            btn.Visible = false;
        }
        else btn.Visible = true;

    }



}