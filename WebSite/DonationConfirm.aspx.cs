﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DonationConfirm : System.Web.UI.Page
{
    private string ContBancar;



    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            // you need to also load the card details if the user had them saved 
            Amount.Text = Request.QueryString["amount"];
            //Amount.Text = Request.QueryString["Amount"];
            string EventId = Request.QueryString["event"];



            if (EventId == null)
                Response.Redirect("EventList");

            string query = "Select EventName,o.BankAccount From Events e Join ONGs o on e.ONGID = o.ONGID Where EventID like @EventId";

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            con.Open();


            SqlCommand cmd = new SqlCommand(query, con);

            try
            {
                cmd.Parameters.AddWithValue("@EventId", EventId);
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr.HasRows)
                    {
                        rdr.Read();
                        Name.InnerText = rdr["EventName"].ToString();
                        ContBancar = rdr["BankAccount"].ToString();

                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                con.Close();
                //con.Dispose();
            }


            string uID = Session["Id"].ToString();
            string query2 = "Select CardHolderName,CardHolderNumber,ExpirationDate,SecurityCode From Users Where UserID like @UserID";

            con.Open();
            SqlCommand cmd2 = new SqlCommand(query2, con);
            cmd2.Parameters.AddWithValue("@UserID", uID);
            try
            {
                using (SqlDataReader rdr = cmd2.ExecuteReader())
                {
                    if (rdr.HasRows)
                    {
                        rdr.Read();
                        CardNumber.Text = rdr["CardHolderNumber"].ToString();
                        CardholderName.Text = rdr["CardHolderName"].ToString();
                        ExpirationDate.Text = rdr["ExpirationDate"].ToString();
                        SecurityCode.Text = rdr["SecurityCode"].ToString();

                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
    }

    protected void Pay(object sender, EventArgs e)
    {
        string EventId = Request.QueryString["event"];

        string query = "Update Events Set Pledged = Pledged + @CurDonation, Backers = Backers + 1 Where EventID like @EventId";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        SqlCommand cmd = new SqlCommand(query, con);

        con.Open();

        try
        {
            cmd.Parameters.AddWithValue("@EventId", EventId);
            cmd.Parameters.AddWithValue("@CurDonation", Amount.Text);
            cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            con.Close();
            con.Dispose();
        }
        Response.Redirect("EventPage.aspx?a=" + EventId);
    }
}