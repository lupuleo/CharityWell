﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RespondToChallenge : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string queryEvent = "SELECT u.Name as uName,e.EventPicture as eEventPicture, e.EventName as eEventName, c.Message as cMessage, c.Amount as cAmount FROM Challanges c JOIN Users u on u.UserID = c.User1ID JOIN Events e on e.EventID = c.EventID WHERE c.ChallengesID = @ChallengeID";

        //temporar pana fac friends and challanges sa arate tot ce trebuie si sa fac sa fie link de la challange pana aici
        string ChallengeID = Request.QueryString["ChallengeId"];
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        SqlCommand command = new SqlCommand(queryEvent, con);
        try
        {
            con.Open();
            command.Parameters.AddWithValue("@ChallengeID", ChallengeID);
            SqlDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                Response.Write("No rows returned");
                return;
            }
            while (reader.Read())
            {
                Username.InnerText = reader["uName"].ToString();
                EventName.Text = reader["eEventName"].ToString();
                EventDescription.Text = reader["cMessage"].ToString();
                ChallengeAmount.Text = reader["cAmount"].ToString();


            }


        }
        catch (Exception ex)
        {
            Response.Write(ex);
        }
        finally
        {
            con.Close();
        }



    }
    protected void Accept(object sender, EventArgs e)
    {

    }
    protected void Decline(object sender, EventArgs e)
    {

    }
    protected void LoadButton(object sender, EventArgs e)
    {
        Control button = (Control)sender;
    }
    protected void Press(object sender, CommandEventArgs e)
    {
        var myAction = e.CommandName as string;
        var myID = int.Parse(e.CommandArgument.ToString());
    }
}