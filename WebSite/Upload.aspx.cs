﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Upload : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
       
    }





    protected void btnUpload_Click(object sender, EventArgs e)

    {

        string FileName = Path.GetFileName(FileUpload1.PostedFile.FileName);



        if (FileName != null)
        {

            //Save files to disk

            FileUpload1.SaveAs(Server.MapPath("Pictures/" + FileName));

            var UserId = Context.User.Identity.GetUserId();
            //Add Entry to DataBase



            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            string strQuery = "insert into Events(Name,ONGId,Description,Goal,Image)" +

                " values(@Name,@ONGId, @Description,@Goal,@Image); SELECT CAST(scope_identity() as int);  ";

            SqlCommand cmd = new SqlCommand(strQuery, con);

            cmd.Parameters.AddWithValue("@FileName", FileName);
            cmd.Parameters.AddWithValue("@Name", EventName.Text);
            cmd.Parameters.AddWithValue("@Image", "Pictures/" + FileName);

            cmd.Parameters.AddWithValue("@ONGId", UserId);
            cmd.Parameters.AddWithValue("@Description", Description.Text);
            cmd.Parameters.AddWithValue("@Goal", GoalAmount.Text);

            cmd.CommandType = CommandType.Text;

            cmd.Connection = con;

            try

            {

                con.Open();

                int photoid = (int)cmd.ExecuteScalar();
                con.Close();

            }

            catch (Exception ex)

            {

                Response.Write(ex.Message);

            }

            finally

            {

                con.Close();

                con.Dispose();


            }

        }

    }

}