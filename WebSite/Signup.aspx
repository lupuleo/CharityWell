﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Signup.aspx.cs" Inherits="Signup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <style>
         
          #box{
              width:500px;
          }
    </style>
    <div id="box">
        
            <h3 style="padding-left: 40px; padding-top:10px">Sign up</h3>
            <div style="padding:30px">
              
                    <div class="form-group">

                        <asp:TextBox  runat="server" Cssclass="form-control" id="Name" aria-describedby="nameHelp" placeholder="Name"/>

                    </div>
                    <div class="form-group">

                        <asp:TextBox  runat="server"  Cssclass="form-control" id="Email" aria-describedby="emailHelp" placeholder="Email"/>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Email not valid" ControlToValidate="Email" ValidationExpression='^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$'></asp:RegularExpressionValidator> 
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Email is required" ControlToValidate="Email"></asp:RequiredFieldValidator>
                     </div>
                    <div class="form-group">
                        <asp:TextBox  runat="server"  Cssclass="form-control" id="ReenterEmail" placeholder="Re-enter email"/>
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Email is required" ControlToValidate="ReenterEmail"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="CompareValidator" ControlToCompare="Email" ControlToValidate="ReenterEmail"></asp:CompareValidator>
                    </div>
                    <div class="form-group">
                        <asp:TextBox runat="server"  Cssclass="form-control" id="Password" placeholder="Password" TextMode="Password" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Password is required" ControlToValidate="Password"></asp:RequiredFieldValidator>
                        </div>
                
                    <div class="form-group">
                        <asp:TextBox runat="server" Cssclass="form-control" id="ReenterPassword" placeholder="Re-enter Password" TextMode="Password" />
                           <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Password is required" ControlToValidate="ReenterPassword"></asp:RequiredFieldValidator>
          <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="The passwords must match" ControlToCompare="Password" ControlToValidate="ReenterPassword"></asp:CompareValidator>
                    </div>
                     <div class="form-group">
                        <asp:label for="FileUpload1" runat="server"><b>Picture</b></asp:label>
                        <br />
                       
                        <asp:FileUpload ID="FileUpload1" runat="server" OnChange ="LoadFile"/>
                           
                    </div>
                    <asp:Button runat="server" id="signupbtn" OnClick="Save" CssClass="btn btn btn-success btn-lg btn-block" Text="Sign me up!" ></asp:Button>
             
              
            </div>
            

        </div>


</asp:Content>

