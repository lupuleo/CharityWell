﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SignupONG : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Save(object sender, EventArgs e)
    {
        string NumeAUX = Name.Text.ToString();
        string EmailAUX = Email.Text.ToString();
        string PasswordAUX = Password.Text.ToString();
        string ContAUX = Cont.Text.ToString();
        string CUIAUX = Cui.Text.ToString();

        string query = "Insert into ONGs(Email,Name,Password,BankAccount,CUI) VALUES (@Email,@Name,@Password,@Cont,@CUI)";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        SqlCommand cmd = new SqlCommand(query, con);

        con.Open();

        try
        {
            cmd.Parameters.AddWithValue("@Name", NumeAUX);
            cmd.Parameters.AddWithValue("@Email", EmailAUX);
            cmd.Parameters.AddWithValue("@Password", PasswordAUX);
            cmd.Parameters.AddWithValue("@Cont", ContAUX);
            cmd.Parameters.AddWithValue("@CUI", CUIAUX);


            cmd.ExecuteNonQuery();


        }

        catch (Exception ex)

        {

            Response.Write(ex.Message);

        }

        finally

        {

            con.Close();



            con.Dispose();

        }
        Response.Redirect("Login");
    }
}