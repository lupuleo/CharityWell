﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FrendsAndChallenges : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        string User1ID = Session["Id"].ToString();
        string query = "SELECT e.EventPicture as eEventPicture, o.Name as ONG, e.EventName as eEventName,u.Name as Name, c.Status as cStatus " +
            "FROM Challanges c " +
            "JOIN Events e on e.EventID = c.EventID " +
            "JOIN ONGs o on o.ONGID = e.ONGID " +
            "JOIN Users u on c.User1ID = u.UserID" +
            "WHERE c.User1ID Like @User1ID";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

        SqlCommand cmd = new SqlCommand(query, con);
        try
        {
            con.Open();
            cmd.Parameters.AddWithValue("@User1ID", User1ID);
            ListViewChallenges.DataSource = cmd.ExecuteReader();
            ListViewChallenges.DataBind();


        }
        catch (Exception ex)
        {
            Response.Write(ex);
        }
        finally
        {
            con.Close();
        }

        query = "SELECT u.Name as uName, u.ProfilePicture as uPicture FROM Friends f JOIN Users u on f.FriendID = u.UserID " +
            "WHERE f.UserID Like @User1ID ";

        cmd = new SqlCommand(query, con);
        try
        {
            con.Open();
            cmd.Parameters.AddWithValue("@User1ID", User1ID);
            ListViewFriends.DataSource = cmd.ExecuteReader();
            ListViewFriends.DataBind();


        }
        catch (Exception ex)
        {
            Response.Write(ex);
        }
        finally
        {
            con.Close();
        }

    }
    protected void LoadButton(object sender, EventArgs e)
    {
        
        string id= ((Button)sender).CommandArgument;
        string query = " SELECT f.Status as Status FROM Friends f JOIN Users u on f.FriendID = u.UserID where f.FriendID LIKE @FID and f.UserID LIKE @UID ";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        SqlCommand cmd = new SqlCommand(query, con);
        cmd.Parameters.AddWithValue("@FID", id);
        cmd.Parameters.AddWithValue("@UID", (string)Session["Id"]);
        con.Open();
        SqlDataReader rdr = cmd.ExecuteReader();
        if (rdr.HasRows)
        {
            rdr.Read();
            if (rdr["Status"].ToString() == "Pending")
                ((Button)sender).Text = "Pending";
            else 
                if(rdr["Status"].ToString() == "Accepted")
                    ((Button)sender).Text = "Chalenge";
            con.Close();
        }
        else
        {
            rdr.Close();
            query = "SELECT f.Status as Status FROM Friends f JOIN Users u on u.UserID = f.UserID where f.UserID LIKE @FID AND f.FriendID LIKE @UID";
            cmd.CommandText = query;
            rdr = cmd.ExecuteReader();
            if(rdr.HasRows)
            {
                rdr.Read();
                if (rdr["Status"].ToString() == "Pending")
                    ((Button)sender).Text = "Accept Request";
            }
            rdr.Close();
            con.Close();
        }
    }
    protected void Press(object sender, CommandEventArgs e)
    {
        var myAction = e.CommandName as string;
        var myID = int.Parse(e.CommandArgument.ToString());
        string text = ((Button)sender).Text;
        if( text == "Accept Request")
        {
            //accept friend request

        }
        else if (text == "Challenge")
        {
            //challenge this person
        }
        else
        {
            //it's pending do nothing
        }
    }
    protected void ViewChallenge(object sender, CommandEventArgs e)
    {
        var myAction = e.CommandName as string;
        var myID = int.Parse(e.CommandArgument.ToString());
        Response.Redirect("RespondToChallenge.aspx?ChallengeId=" + myID);
    }
}