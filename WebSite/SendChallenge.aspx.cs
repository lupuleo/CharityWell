﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SendChallenge : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string ID = Session["Id"].ToString();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        string strQuery = "select u.UserId as UserID,u.Name as Name from Users u JOIN Friends f on f.UserID = u.UserID  where u.UserID LIKE @User union SELECT u.UserID as UserID,u.Name as Name FROM Friends f JOIN Users u on u.UserID = f.UserID where f.FriendID LIKE @User ";
        string EventId = Request.QueryString["EventId"];
        string EventQuery = "SELECT EventName FROM Events WHERE EventId LIKE @EventID";

        SqlCommand cmd = new SqlCommand(EventQuery, con);
        try
        {
            con.Open();
            cmd.Parameters.AddWithValue("@EventID", EventId);
            SqlDataReader reader = cmd.ExecuteReader();
            if (!reader.HasRows)
            {
                EventName.Text = "Fail";
                return;
            }
            while (reader.Read())
            {
                string test = reader["EventName"].ToString();
                EventName.Text = test;
            }

        }
        catch (Exception ex)
        {
            Response.Write(ex);

        }
        finally
        {
            con.Close();
        }


        try
        {
            cmd = new SqlCommand(strQuery, con);

            cmd.Parameters.AddWithValue("@User", ID);
            con.Open();
            DDLFriends.DataSource = cmd.ExecuteReader();

            DDLFriends.DataTextField = "Name";
            DDLFriends.DataValueField = "UserID";
            DDLFriends.DataBind();



        }
        catch (Exception ex)
        {
            Response.Write(ex);
        }
        finally
        {
            con.Close();
        }




        }
        catch (Exception ex)
        {
            Response.Write(ex);
        }
        finally
        {
            con.Close();
        }
        
        
    }
    protected void Send(object sender, EventArgs e)
    {
        string ID = Session["Id"].ToString();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        string EventId = Request.QueryString["EventId"];

        string query = "INSERT into Challanges(User1ID,User2ID,EventID,Message,Amount,Status) " +
           "values(@User1ID,@User2ID,@EventID,@Message,@Amount,@Status)";

        SqlCommand cmd = new SqlCommand(query, con);

        try
        {
            con.Open();
            cmd.Parameters.AddWithValue("@User1ID", ID);
            cmd.Parameters.AddWithValue("@User2ID", DDLFriends.SelectedValue);
            cmd.Parameters.AddWithValue("@EventID", EventId);
            cmd.Parameters.AddWithValue("@Message", ChallengeDescription.Text);
            cmd.Parameters.AddWithValue("@Amount", Amount.Text);
            cmd.Parameters.AddWithValue("@Status", "Pending");

            cmd.ExecuteNonQuery();


        }
        catch (Exception ex)
        {
            Response.Write(ex);
        }
        finally
        {
            con.Close();
            Response.Redirect("FrendsAndChallenges.aspx");
        }



    }
    protected void LoadButton(object sender, EventArgs e)
    {
        Control button = (Control)sender;
    }
    protected void Press(object sender, CommandEventArgs e)
    {
        var myAction = e.CommandName as string;
        var myID = int.Parse(e.CommandArgument.ToString());
    }
}