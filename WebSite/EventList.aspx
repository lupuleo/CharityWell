﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="EventList.aspx.cs" Inherits="EventList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
   <style>
    .item {
            background-color: white;
            border: 1px solid;
            padding: 10px;
            box-shadow: 5px 10px 8px #888888;
        }
    hr {
    display: block;
    height: 1px;
    border: 0;
    border-top: 1px solid #ccc;
    margin: 1em 0;
    padding: 0;
}
        .img-circle {
        border-radius: 50%;
    }
    </style> 
    <div class="container">
       <h1 id="Name" runat="server" style="padding-left:30%; padding-top: 5%; color:cornflowerblue">Event List</h1>
         <div class="row">
       <asp:ListView ID="ListView" 
        GroupItemCount="3"
        runat="server">
        <LayoutTemplate>
          <table cellpadding="20" width="900px" runat="server" id="DonationOptions"> <!-- how much space between the eventss -->
           
            <tr runat="server" id="groupPlaceholder" />
          </table>
        </LayoutTemplate>
        <GroupTemplate>
          <tr runat="server" id="DonationsRow">
            <td runat="server" id="itemPlaceholder" />
          </tr>
        </GroupTemplate>
        <GroupSeparatorTemplate>
          <tr runat="server">
            <td colspan="6"><hr /></td>
          </tr>
        </GroupSeparatorTemplate>
        <ItemTemplate>
            <td align="center" class="item" style="padding:20px; width: 300px" runat="server">
                  <div class="row">
                   
           
             <a href='EventPage.aspx?a=<%#Eval("EventID") %>'>  <%# Eval("EventName")%> <br /> </a>
             ONG:  <%# Eval("Name")%>
            </div>
                <hr />
<a href='EventPage.aspx?a=<%#Eval("EventID") %>'><img alt=""   src='<%# Eval("EventPicture") %>' height="100" width="150" /> </a>
                <hr />
               
              
                <asp:Button type="button" CssClass="btn btn-succes btn-lg btn-block" runat="server" OnCommand="DonateForThis" CommandName="DonateForThis"
                    CommandArgument='<%#Eval("EventID")%>' OnPreRender="checkIfLogged" Text="Donate"></asp:Button> <br />
                <asp:Button type="button" CssClass="btn btn-succes btn-lg btn-block" runat="server" OnCommand="ChallengeForThis" CommandName="ChallengeForThis"
                    CommandArgument='<%#Eval("EventID")%>' OnPreRender="checkIfLogged" Text="Challenge"></asp:Button>
            </td>
        </ItemTemplate>
        <ItemSeparatorTemplate>
          <td class="separator" runat="server">&nbsp;</td>
        </ItemSeparatorTemplate>
      </asp:ListView>

           

    </div>
</div>



   

</asp:Content>

