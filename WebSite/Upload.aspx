﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Upload.aspx.cs" Inherits="Upload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <style>
        .round {
            border-radius: 5px 10px 15px 20px;
            background-color: white;
            border-color: grey;
            border-width: thin;
            width: 30%;
            position: relative;
            left: 400px;
            padding-top: 4%;
        }
            
    </style>

    <div class="row">
        <div class="round">


            <div class="form-group">
                <asp:Label runat="server" Font-Bold ="true">Event Name</asp:Label>
                <asp:TextBox ID="EventName" runat="server" CssClass="form-control" />
            </div>
            <div class="form-group">
                <asp:Label runat="server" Font-Bold ="true">Goal Amount</asp:Label>
                <asp:TextBox ID="GoalAmount" runat="server" CssClass="form-control" TextMode="Number"/>

            </div>
            <div class="form-group">
                <asp:Label runat="server" Font-Bold ="true">Event Image</asp:Label>
                
                </div>
            
                <asp:FileUpload ID="FileUpload1" runat="server" OnChange="LoadFile" cssClass="form-control input-lg"/>
                <br />
            
            <div class="form-group">
                <asp:Label runat="server" Font-Bold ="true">Description</asp:Label>
                <asp:TextBox ID="Description" runat="server" CssClass="form-control" />
            
            </div>
            
                <asp:Button ID="btnUpload" runat="server" CssClass="btn btn-default" Text="Create Event" OnClick="btnUpload_Click" />
            

        </div>
    </div>

    

</asp:Content>

