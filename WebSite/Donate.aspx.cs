﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Donate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            int id = int.Parse(Request.QueryString["event"]);
            string searchText = Request.QueryString["searchText"];
            if (searchText == null)
                searchText = "";

            string query = "SELECT * from Milestones WHERE EventID = @EventID; ";


            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            con.Open();

            try
            {
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@EventID", id);
                SqlDataReader reader = com.ExecuteReader();
                ListView.DataSource = reader;
                ListView.DataBind();

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
    }
    protected void DonateForThis(object sender, CommandEventArgs e)
    {
        string donationAmount = e.CommandArgument as string;

        if (donationAmount == "none")
        {
            donationAmount = FreeTextAmount.Text.ToString();
        }
        string id = Request.QueryString["event"];

        if ((string)Session["Type"] == "User")
            Response.Redirect("DonationConfirm.aspx?event=" + id + "&amount=" + donationAmount);

        else Response.Redirect("Signup");
    }
}