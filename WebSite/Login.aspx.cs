﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : System.Web.UI.Page
{
    //string IDAUX;
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {

            WrongData.Visible = false;
            
        }
        ChooseOne.Visible = false;
    }
    protected void LoginClick(object sender, EventArgs e)
    {
        string IDAUX;

        if (RadioButtonList.SelectedIndex > -1)
        {
           
            if(RadioButtonList.SelectedItem.Text == "Log in as User")
            {
                IDAUX = VerifUser();
                if (IDAUX != "")
                {
                    Session["Type"] = "User";
                    Session["Id"] = IDAUX;
                    Response.Redirect("EventList");
                }
                else
                    WrongData.Visible = true;
            }
            else
       if (RadioButtonList.SelectedItem.Text == "Log in as ONG")
            {
                IDAUX = VerifONG();
                if (IDAUX != "")
                {
                    Session["Type"] = "ONG";
                    Session["Id"] = IDAUX;
                    Response.Redirect("EventList");
                }
                else
                    WrongData.Visible = true;
            }

        }
        else
        {
            ChooseOne.Visible = true;
            
        }
       
        

            

        //cauta unde gaseste emailul si parola, daca il gaseste in user atunci 
        //Session["Type"] = "User";
        //Session["Id"] = "UserId";

        //daca il gaseste in ONG
        //Session["Type"] = "ONG";
        //Session["Id"] = "ONGId";

        //Pt. logg off se face Session.Abandon(); 
        //in loc de user.context acolo ca sa obtii id-ul faci (string) Session["Id"];


    }

    protected string VerifUser()
    {
        string EmailAUX = InputEmail.Text.ToString();
        string PasswordAUX = InputPassword.Text.ToString();
        string IDAUX = "";


        string query = "SELECT UserID FROM Users WHERE Email like @Email AND Password like @Password";
        // Deschidem conexiunea la baza de date
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        SqlCommand cmd = new SqlCommand(query, con);
        con.Open();

        try
        {
            // Se construieste comanda SQL
            
            cmd.Parameters.AddWithValue("@Email", EmailAUX);
            cmd.Parameters.AddWithValue("@Password", PasswordAUX);
            // Se executa comanda si se returneaza valorile intr-un reader
            //Int32 count = (Int32)cmd.ExecuteScalar();
            //IDAUX = count.ToString();
            SqlDataReader reader = cmd.ExecuteReader();
            
            // Citim rand cu rand din baza de date
            while (reader.Read())
            {
                IDAUX = reader["UserID"].ToString();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            con.Close();
        }


        return IDAUX;
    }

    protected string VerifONG()
    {
        string EmailAUX = InputEmail.Text.ToString();
        string PasswordAUX = InputPassword.Text.ToString();
        string IDAUX = "";


        string query = "SELECT ONGID"
                + " FROM ONGs"
                + " WHERE Email like @Email AND Password like @Password";

        // Deschidem conexiunea la baza de date
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        con.Open();

        try
        {
            // Se construieste comanda SQL
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("@Email", EmailAUX);
            com.Parameters.AddWithValue("@Password", PasswordAUX);
            // Se executa comanda si se returneaza valorile intr-un reader
            SqlDataReader reader = com.ExecuteReader();

            // Citim rand cu rand din baza de date
            while (reader.Read())
            {
                IDAUX = reader["ONGID"].ToString();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            con.Close();
        }


        return IDAUX;
    }
}
