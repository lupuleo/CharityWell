﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master" CodeFile="CreateEvent.aspx.cs" Inherits="CreateEvent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <script src='//production-assets.codepen.io/assets/editor/live/console_runner-079c09a0e3b9ff743e39ee2d5637b9216b3545af0de366d4b9aad9dc87e26bfd.js'></script>
    <script src='//production-assets.codepen.io/assets/editor/live/events_runner-73716630c22bbc8cff4bd0f07b135f00a0bdc5d14629260c3ec49e5606f98fdd.js'></script>
    <script src='//production-assets.codepen.io/assets/editor/live/css_live_reload_init-2c0dc5167d60a5af3ee189d570b1835129687ea2a61bee3513dee3a50c115a77.js'></script>
    <meta charset='UTF-8'>
    <meta name="robots" content="noindex">
    <link rel="canonical" href="https://codepen.io/nilsynils/pen/VKQwBJ?depth=everything&order=popularity&page=61&q=tools&show_forks=false" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css' />
    <style>
        body {
            font: 16px "Open Sans";
            color: darkSlateGrey;
        }
    </style>

    <style class="cp-pen-styles">
        body {
            background: aliceBlue;
        }

        .wrapper {
            width: 80%;
            max-width: 800px;
            background: #fff;
            margin: 3em auto;
            padding: 1em 2em;
            border: 1px solid lightGrey;
            border-radius: 3px;
            position: relative;
        }

            .wrapper h2 {
                margin-top: 0;
            }

        .tools {
            padding: 0;
            list-style-type: none;
            display: inline-flex;
            flex-wrap: wrap;
            padding: .5em .5em .5em 0;
            margin: 0;
        }

            .tools li {
                margin: 0 1em 0 0;
                color: grey;
            }

                .tools li a {
                    color: grey;
                }

        .editableContent *::selection {
            background: #9dcaff;
        }

        .editableContent:focus {
            outline: none;
        }

        .modal {
            position: absolute;
            left: 11em;
            top: 4.5em;
            display: none;
        }

        .modal__wrapper {
            background: #fff;
            padding: 0 .5em;
            border: 1px solid lightGrey;
            border-radius: 3px;
            transition: all 1s;
            position: relative;
            width: 22em;
        }

            .modal__wrapper:after, .modal__wrapper:before {
                right: 100%;
                top: 50%;
                border: solid transparent;
                content: " ";
                height: 0;
                width: 0;
                position: absolute;
                pointer-events: none;
            }

            .modal__wrapper:after {
                border-color: rgba(255, 255, 255, 0);
                border-right-color: #fff;
                border-width: 6px;
                margin-top: -6px;
            }

            .modal__wrapper:before {
                border-color: transparent;
                border-right-color: lightGrey;
                border-width: 7px;
                margin-top: -7px;
            }

        .modal input {
            height: 1.5em;
            padding: .25em;
            width: 20em;
            font-size: 16px;
            border: 0;
        }

            .modal input:focus {
                outline: none;
            }

        .visible {
            display: block;
        }

        a.highlighted {
            background: blue;
            color: white;
        }

        .editableContent a:hover {
            cursor: pointer;
        }

        .linkWrapper {
            position: relative;
        }

        .hoverPop {
            position: absolute;
            left: 0;
            top: 2.2em;
            display: block;
        }

        .hoverPop__wrapper {
            background: #fff;
            padding: .5em .5em;
            border: 1px solid lightGrey;
            border-radius: 3px;
            transition: all 1s;
            position: relative;
            width: auto;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);
        }

            .hoverPop__wrapper:after, .hoverPop__wrapper:before {
                bottom: 100%;
                left: 50%;
                border: solid transparent;
                content: " ";
                height: 0;
                width: 0;
                position: absolute;
                pointer-events: none;
            }

            .hoverPop__wrapper:after {
                border-color: rgba(255, 255, 255, 0);
                border-bottom-color: #fff;
                border-width: 6px;
                margin-left: -6px;
            }

            .hoverPop__wrapper:before {
                border-color: transparent;
                border-bottom-color: lightGrey;
                border-width: 7px;
                margin-left: -7px;
            }
    </style>

    <style>
        .button4 {
            border-radius: 12px;
        }

        .navbar-nav.navbar-center {
            position: absolute;
            left: 50%;
        }

        body {
            background-color: white;
        }

        nav {
            background-color: white;
            border: 1px solid;
            padding: 10px;
            box-shadow: 5px 10px 8px #888888;
        }

        #box {
            background-color: white;
            width: 800px;
            height: 800px;
            position: absolute;
            left: 20%;
            top: 10%;
        }
    </style>


    <!-- navbar on top -->


    <!-- the main box in the middle -->

    <div id="box">

        <h2 style="padding-left: 40px; padding-top: 10px; color: cornflowerblue">Create event</h2>
        <div style="padding: 30px">

            <div class="form-group">
                <asp:Label for="EventName" runat="server"><b>Event Name</b></asp:Label>
                <asp:TextBox runat="server" type="text" CssClass="form-control" ID="EventName" aria-describedby="nameHelp" placeholder="Name" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="EventName" ErrorMessage="It needs a name"></asp:RequiredFieldValidator>
            </div>
            <div class="form-group">
                <asp:Label for="GoalAmount" runat="server"><b>Goal Amount</b></asp:Label>
                <asp:TextBox runat="server" type="text" class="form-control" ID="GoalAmount" aria-describedby="emailHelp" placeholder="$" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="GoalAmount" ErrorMessage="Introduceti un nume"></asp:RequiredFieldValidator>
            </div>
            <br />
            <%--<asp:Button ID="btnUpload" runat="server" CssClass="btn btn-primary" Text="Browse" OnClick="btnUpload_Click" />--%>







           

            <div class="form-group">
                <asp:Label for="Descriere" runat="server"><b>Descriere</b></asp:Label>
                <asp:TextBox runat="server" type="text" class="form-control" TextMode="MultiLine" ID="DescriereTemp" aria-describedby="emailHelp" placeholder="" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="DescriereTemp" ErrorMessage="Nu poate fi gol"></asp:RequiredFieldValidator>
            </div>

      

            <!--tabelul cu milestone -->

            <br />
            <b>Event Milestones</b>

            <asp:Table ID="myTable" CssClass="table" runat="server" Width="50%">
                <asp:TableRow>
                    <asp:TableCell>Amount</asp:TableCell>
                    <asp:TableCell>Reward</asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <!-- milestone 1 --> 

            <div class="form-group row">
                <div class="col-xs-3">
                    <asp:TextBox runat="server" type="text" CssClass="form-control" ID="Amount1" placeholder="Amount" />
                </div>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Amount-ul trebuie sa fie un numar" ValidationExpression="(^$)|(^[0-9]+$)" ControlToValidate="Amount2"></asp:RegularExpressionValidator>
                <div class="col-xs-3">

                    <asp:TextBox runat="server" type="text" CssClass="form-control" ID="Reward1" placeholder="Reward" />

                </div>
              
            </div>
            <br />
            <!-- milestone 2 -->
             <div class="form-group row">
                <div class="col-xs-3">
                    <asp:TextBox runat="server" type="text" CssClass="form-control" ID="Amount2" placeholder="Amount" />
                </div>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Amount-ul trebuie sa fie un numar" ValidationExpression="(^$)|(^[0-9]+$)" ControlToValidate="Amount2"></asp:RegularExpressionValidator>
                <div class="col-xs-3">

                    <asp:TextBox runat="server" type="text" CssClass="form-control" ID="Reward2" placeholder="Reward" />

                </div>
               
            </div>
            <br />

             <!-- milestone 3 -->
             <div class="form-group row">
                <div class="col-xs-3">
                    <asp:TextBox runat="server" type="text" CssClass="form-control" ID="Amount3" placeholder="Amount" />
                </div>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Amount-ul trebuie sa fie un numar" ValidationExpression="(^$)|(^[0-9]+$)" ControlToValidate="Amount3"></asp:RegularExpressionValidator>
                <div class="col-xs-3">

                    <asp:TextBox runat="server" type="text" CssClass="form-control" ID="Reward3" placeholder="Reward" />

                </div>
               
            </div>
            <br />

             <!-- milestone 4 -->
             <div class="form-group row">
                <div class="col-xs-3">
                    <asp:TextBox runat="server" type="text" CssClass="form-control" ID="Amount4" placeholder="Amount" />
                </div>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Amount-ul trebuie sa fie un numar" ValidationExpression="(^$)|(^[0-9]+$)" ControlToValidate="Amount4"></asp:RegularExpressionValidator>
                <div class="col-xs-3">

                    <asp:TextBox runat="server" type="text" CssClass="form-control" ID="Reward4" placeholder="Reward" />

                </div>
               
            </div>
            <br />

            <!--Bugged -->            
            <div class="form-group">
                <asp:Label for="FileUpload1" runat="server"><b>Picture</b></asp:Label>
                <br />

                <asp:FileUpload ID="FileUpload1" runat="server" />

            </div>
            <br />
            <asp:Button Style="padding-bottom: 10px" runat="server" OnClick="btnUpload_Click" type="button" ID="createbtn" class="btn btn btn-success btn-lg btn-block" Text="Create!" />

        </div>
    </div>
    <br />
    <br />

</asp:Content>
