﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class EventList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            string searchText = Request.QueryString["searchText"];
            if (searchText == null)
                searchText = "";

            string query = "Select EventID,EventName,Description,Goal,Pledged,Backers,EventPicture,Name " +
                            "From Events e Join ONGs o on e.ONGID = o.ONGID " +
                            "where lower(EventName) LIKE '%" + searchText.ToLower() + "%' OR lower(Name) LIKE '%" + searchText.ToLower() + "%';";



            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            con.Open();

            try
            {
                SqlCommand com = new SqlCommand(query, con);
                SqlDataReader reader = com.ExecuteReader();
                ListView.DataSource = reader;
                ListView.DataBind();

            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
    }

    protected void DonateForThis(object sender, CommandEventArgs e)
    {
        var myAction = e.CommandName as string;
        var myID = int.Parse(e.CommandArgument.ToString());

        if ((string)Session["Type"] == "User")
            Response.Redirect("Donate.aspx?event=" + myID);

        else Response.Redirect("Signup");
    }
    protected void ChallengeForThis(object sender, CommandEventArgs e)
    {

        var myAction = e.CommandName as string;
        var myID = int.Parse(e.CommandArgument.ToString());

        Response.Redirect("SendChallenge.aspx?EventId=" + myID);

    }

    protected void checkIfLogged(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        if (!((string)Session["Type"] == "User"))
        {

            btn.Visible = false;
        }
        else btn.Visible = true;

    }
}