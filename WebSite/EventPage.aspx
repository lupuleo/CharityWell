﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="EventPage.aspx.cs" Inherits="EventPage" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
   
     <div class="container" style="padding-top:5%">
         <h1 id="Name" runat="server" style="padding-left:30%; color:cornflowerblue"></h1>
        <div class="row" style="padding-top:5%" >
            <div class="col-md-8">
                <asp:Image ID="Image" runat="server" Height="50%" Width="70%"  />


            </div>
            <div class="col-md-4" >
                 <div class="progress">
                <div class="progress-bar" id="ProgressBar" role="progressbar" aria-valuenow="0"
                aria-valuemin="0" aria-valuemax="100" style="width:70%">
                 
                </div>
                </div> 
                <br />
               
                <div style="color:cornflowerblue"> US$ <h5  runat="server" style="display: inline" id="Current" ></h5></div>
                <div style="color:darkgrey"> pledged of US$<h5 Id="Target" style="display: inline" runat="server" ></h5> goal </div>
                <br />
                <asp:label runat="server" style="color:cornflowerblue" ID="Backers" ></asp:label>
                 <div style="color:darkgrey">Backers</div>
                <br />
                <asp:Button runat="server" Onclick="DonateForThis"  OnPreRender="checkIfLogged" CssClass="btn btn-light btn-lg btn-block" Text="Donate" />
                  <asp:Button runat="server" OnClick="ChallengeForThis" OnPreRender="checkIfLogged" CssClass="btn btn-light btn-lg btn-block" Text="Challenge" />
               
                
                    <asp:Image runat="server" ImageUrl="Pictures/facebook.png" Width="50" Height="50" style="display:inline-block" />
                    <asp:Image runat="server" ImageUrl="Pictures/twitter.png" Width="50" Height="50" style="display:inline-block" />
                      <asp:Image runat="server" ImageUrl="Pictures/mail.png" Width="50" Height="50" style="display:inline-block" />
                </div>
            </div>
        

          <div class="row" style="padding-top:30px; padding-left:15px">
              <div class="col-md-8">
                <div style="color:cornflowerblue"> Organised by <p id="ONG" style="display:inline-block" runat="server"></p> </div>
                   <p id="Description" runat="server"></p>
             </div>
              <div class="col-md-4">
                    <asp:ListView ID="ListView" runat="server" AutoGenerateColumns="false" ShowHeader="False">
                        <LayoutTemplate>
                        <ul style="list-style: none" runat="server">
                         <asp:PlaceHolder ID="itemPlaceholder" runat="server" />

                        </ul>
                        </LayoutTemplate>
                        <ItemTemplate>
                    <li runat="server" style="background-color:darkgrey; width:200px; height:100px"><b>Pledge <%#Eval("Amount")%> </b> <br />
                        <%#Eval("Reward")%>
                    </li>
          </ItemTemplate>
         <EmptyDataTemplate>
             
         </EmptyDataTemplate>      
                
            </asp:ListView>
              </div>

          </div>
          </div>     
    
      
    <script>
        
      
     
        $(window).on('load', function () {
            var x = $('h5').first().text();
            var y = $('h5').last().text();
            var percent = (Number(x) / Number(y))*100;
           
           $('#ProgressBar').css('width', percent+'%');
        });
    </script>
</asp:Content>

