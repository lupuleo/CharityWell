﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ChallengeOutcome.aspx.cs" Inherits="ChallengeOutcome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
      <style>
  #friendlist{width: 500px;
                height: 500px;
                background-color:white;
                overflow-y: auto;
                max-width:500px;
                max-height:500px;
                }
  .tooltip {
 width: 200px;
 position: fixed;
 top:100px;
 bottom:0px;
 right:0px;
 left:auto;
}
  
   .shadow {
   -moz-box-shadow:    inset 0 0 10px #000000;
   -webkit-box-shadow: inset 0 0 10px #000000;
   box-shadow:         inset 0 0 10px #000000;
}

   hr {
    display: block;
    background-color:white;
    margin:0px;
    padding:0px;
    height: 1px;
    border: 0;
    border-top: 1px solid #ccc;
    
    padding: 0;
}
   .img-circle {
        border-radius: 50%;
    }
    .rcorners1 {
            border-radius: 25px;
            background: #73AD21;
            padding: 20px;
            width: 200px;
            height: 150px;
        }




   </style>
    <div class="container" style="padding-top:5%">
        <div class="row">
            <div class="col-md-8">
                 <div style="padding-top:2%; padding-left:20%">
                    <h2 style="color:cornflowerblue">Respond to challenge</h2>
                   <br />
                    <div class="row">
                        <div class="col-md-2">
                              <asp:Image ImageUrl='<%# Eval("Image") %>' runat="server" style="margin:10px" class="img-circle" Height="80" Width="80" />
                        </div>
                        <div class="col-md-10">
                            <div style="position:absolute; top:25px; padding-left:0; margin-left:0" ><h3 runat="server" style="color:darkgrey" id="Username">Name</h3></div>
                        </div>
                    </div>
                    <div style="background-color:#d9d9d9; width:300px">
                        <h5 style="padding-left:10px;padding-top:5px">Event</h5>
                        <hr />
                        <br />
                        <asp:Label ID="EventName" style="padding-left:10px;" runat="server" >Somethinggg</asp:Label>
                        <br />
                        <br />

                        <h5 style="padding-left:10px;">Description</h5>
                        <hr />
                        <br />
                        <asp:Label ID="EventDescription" style="padding-left:10px;" runat="server" >Something else</asp:Label>
                        <br />
                        <br />
                        <h5 style="padding-left:10px;">Amount</h5>
                        <hr />
                        <br />
                        <asp:Label ID="ChallengeAmount" style="padding-left:10px;" runat="server" >30</asp:Label>$
                        <br />
                        <br />
                        <div class="row" style="width:300px">
                            <div class="col-md-6">
                                <div style="background-color:#ea4b4b;height:50px; width:150px; ">
                                  <h5 onclick="Decline" style="color:white;  padding-top:15px; padding-left:50px; ">Decline Outcome</h5>
                                </div>
                            </div>
                            <div class="col-md-6">
                                  <div style="background-color:#368bd1; height:50px; width:150px; ">
                                     <h5 runat="server" onclick="Accept" style="color:white; padding-top:15px; padding-left:50px;  ">Confirm Outcome</h5>
                                   </div>
                                </div>
                          
                        </div>
                    </div>
                  
               </div>
            </div>
            <div class="col-md-4" >
                <div style="position: fixed;" >
                      <div style=" width:400px; height:50px" >
                    <h2 style="color:cornflowerblue; text-align:center;   font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif">Friends and challenges</h2>
                   </div>
                 <div class="row">
                    <div id="friendlist">

                 <asp:ListView ID="ListView" runat="server" AutoGenerateColumns="false" ShowHeader="False">
                 <LayoutTemplate>
          <ul style="list-style: none; padding-top:0;">
             <asp:PlaceHolder ID="itemPlaceholder" runat="server" />

         </ul>
     </LayoutTemplate>
     <ItemTemplate>
        <li class="rcorners1" style="background-color:#F5F5DC;  width:400px; height:100px; margin: 1em 0; display: block; padding: 0;">
             <div class="row">
                 <div class="col-md-4">
                       <asp:Image ImageUrl='<%#Eval("Picture")%>' runat="server" style="margin:10px" class="img-circle" Height="80" Width="80" />
                 </div>
                 <div class="col-md-8">
                     <%#Eval("Name")%>
                     <br />
                     <br />
                     <asp:Button runat="server" CssClass="btn btn-primary" OnCommand="Press" CommandArgument='<%#Eval("Id")%>' OnPreRender="LoadButton" Text="Buton"/>
                 </div>
             </div>
           


         </li>
          
         
              </ItemTemplate>
         <EmptyDataTemplate>
             <div>
                 <div class="rcorners1" style="background-color:#F5F5DC;  width:400px; height:100px; margin: 1em 0; display: block; padding: 0;">There is nobody in your friends list :(  </div>
             </div>
         </EmptyDataTemplate>      
                
            </asp:ListView>
       
                </div>
                    </div>
            </div>
    </div>
</div>
        </div>
</asp:Content>

