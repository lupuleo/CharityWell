﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        #wrapper {
            padding-left: 250px;
            transition: all 0.4s ease 0s;
        }

        #sidebar-wrapper {
            margin-left: -10px;
            left: 10px;
            width: 165px;
            background: #0d0d0d;
            position: fixed;
            height: 100%;
            overflow-y: auto;
            z-index: 1000;
            transition: all 0.4s ease 0s;
        }

        #page-content-wrapper {
            width: 100%;
        }

        .sidebar-nav {
            position: absolute;
            top: 0;
            width: 250px;
            list-style: none;
            float:right;
        }

        @media (max-width:800px) {

            #wrapper {
                padding-left: 0;
            }

            #sidebar-wrapper {
                left: 0;
            }

            #wrapper.active {
                position: relative;
                left: 250px;
            }

                #wrapper.active #sidebar-wrapper {
                    left: 250px;
                    width: 250px;
                    transition: all 0.4s ease 0s;
            }
        }
    </style>









    <div id="wrapper">
        
        <div id="page-content-wrapper">
            <div class="page-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:ListView ID="ListViewDEF" runat="server" AutoGenerateColumns="false" ShowHeader="False" EnableViewState="false">
                                <LayoutTemplate>
                                    <ul style="list-style: none" runat="server">
                                        <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                                    </ul>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <h3>
                                        Nume event: <%#Eval("Name" )%>
                                    </h3>
                                     <a href='EventPage.aspx?a=<%# Eval("Id") %>'>
                                        <img alt="" src='<%# Eval("Image") %>' height="300px" width="200px" />
                                        <br />
                                        
                                    </a>
                                        
                                </ItemTemplate>

                            </asp:ListView>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>



</asp:Content>
