﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PaymentSettings : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        

    }
    protected void Save(object sender, EventArgs e)
    {

        var CardHolderNumberAUX = CardNumber.Text;
        var CardHolderNameAUX = CardholderName.Text;
        var CardExpirationDateAUX = ExpirationDate.Text;
        var CardSecurityCodeAUX = SecurityCode.Text;

        /*string FileName = Path.GetFileName(FileUpload1.PostedFile.FileName);

        if (FileName != null)
        {

            //Save files to disk

            FileUpload1.SaveAs(Server.MapPath("Pictures/" + FileName));

            var UserId = Context.User.Identity.GetUserId();
        }
        Image ProfilePicture = 
        */
        string query = "Update Users set CardHolderNumber=@Number,CardHolderName=@CardName,CardExpiration=@CardExpirationDate,SecurityCode=@SecurityCode WHERE UserID like @UserId";
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        SqlCommand cmd = new SqlCommand(query, con);

        con.Open();

        try
        {

            cmd.Parameters.AddWithValue("@UserId", Context.User.Identity.GetUserId());
            cmd.Parameters.AddWithValue("@Number", CardHolderNumberAUX);
            cmd.Parameters.AddWithValue("@CardName", CardHolderNameAUX);
            cmd.Parameters.AddWithValue("@CardExpirationDate", CardExpirationDateAUX); 
            cmd.Parameters.AddWithValue("@SecurityCode", CardSecurityCodeAUX);
            
            cmd.ExecuteNonQuery();


        }

        catch (Exception ex)

        {

            Response.Write(ex.Message);

        }

        finally

        {

            con.Close();



            con.Dispose();

        }
        Response.Redirect(Request.RawUrl);
    }
}